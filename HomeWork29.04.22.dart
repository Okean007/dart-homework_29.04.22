import 'dart:io';

const Map currencySell = {
  'USD': 83.67,
  'EURO': 86.83,
  'RUB': 1.1600,
  'KZT': 0.1967
};
const Map currencyBuy = {
  'USD': 82.33,
  'EURO': 85.50,
  'RUB': 1.0230,
  'KZT': 0.1178
};
void main(List<String> args) {
  print('Курс на сегодня');
  infoSell();
  infoBuy();
  print(
      '\n1-Хотите обменять другую валюту на сом(Покупка)!\n2-Хотите обменять сом на другую валюту(Продажа)!');
  String choise = stdin.readLineSync()!;
  if (choise == '1') {
    bought();
  } else if (choise == '2') {
    toSell();
  }
}

infoSell() {
  print('Продажа валюты');
  currencySell.forEach((key, value) {
    print('$key: $value');
  });
}

infoBuy() {
  print('Покупка валюты');
  currencyBuy.forEach((key, value) {
    print('$key: $value');
  });
}

toSell() {
  String valute = ('Выберите валюту:\nUSD\nEUR\nRUB\nKZT');
  print(valute);

  double sum = 0;
  double result = 0;
  String choise = stdin.readLineSync()!;

  if (choise.toUpperCase() == 'USD') {
    print('сколько долларов вы хотите купить?');
    sum = currencySell['USD'];
  } else if (choise.toUpperCase() == 'EUR') {
    print('сколько евро вы хотите купить?');
    sum = currencySell['EURO'];
  } else if (choise.toUpperCase() == 'RUB') {
    print('сколько рублей вы хотите купить?');
    sum = currencySell['RUB'];
  } else if (choise.toUpperCase() == 'KZT') {
    print('сколько тенге вы хотите купить?');
    sum = currencySell['KZT'];
  } else {
    print('Извините такой валюты у нас нету');
    exit(0);
  }
  int money = int.parse(stdin.readLineSync()!);
  result = money.toDouble() * sum;
  print('$result сом на $money ${choise.toUpperCase()}');
}

bought() {
  String valute = ('Какая у вас валюта:\nUSD\nEUR\nRUB\nKZT');
  print(valute);
  double result = 0;
  double sum = 0;
  String choise = stdin.readLineSync()!;
  if (choise.toUpperCase() == 'USD') {
    print('Сколько Долларов хотите продать?');
    sum = currencyBuy['USD'];
  } else if (choise.toUpperCase() == 'EUR') {
    print('Сколько Евро хотите продать?');
    sum = currencyBuy['EURO'];
  } else if (choise.toUpperCase() == 'RUB') {
    print('Сколько Рублей хотите продать?');
    sum = currencyBuy['RUB'];
  } else if (choise.toUpperCase() == 'KZT') {
    print('Сколько Тенге хотите продать?');
    sum = currencyBuy['KZT'];
  } else {
    print('Извините такой валюты у нас нету');
    exit(0);
  }
  int money = int.parse(stdin.readLineSync()!);
  result = money.toDouble() * sum;
  print('Обмен: на $money ${choise.toUpperCase()} $result Сом');
}
